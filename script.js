let draggingItem;

document.querySelector('.js-container').addEventListener('dragstart', (e) => {
    console.log(e.type, e.target, e);
    draggingItem = e.target;
    console.log(e.target);
});

document.querySelector('.js-container').addEventListener('dragenter', (e) => {
    e.preventDefault();
    console.log(e.type, e.target, e);
});

document.querySelector('.js-container').addEventListener('dragover', (e) => {
    e.preventDefault();
    // console.log(e.type, e.target, e);
});

document.querySelector('.js-container').addEventListener('drop', (e) => {
    e.preventDefault();
    console.log(e.type, e.target, e);

    if (e.target.classList.contains('js-item')) {
        console.log('배달옴: ', draggingItem);
        e.target.after(draggingItem);
    }
});